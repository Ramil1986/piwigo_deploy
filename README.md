# Инфраструктура

На Хост машине произвести установку приложений:
- gitlab-runner

Затем подключить gitlab-runner к проекту.

Также для работы проекта необходимо настроить Variables gitlab:
- AWS_ACCESS_KEY_ID (access key сервисного аккаунта, который будт работать с s3 backend)
- AWS_SECRET_ACCESS_KEY (secret key сервисного аккаунта, который будт работать с s3 backend)
- YC_KEY (key сервисного аккаунта, который будtт работать с yandex cloud)
- yandex_token (токен для работы с yandexcloud)


## Vars

Для корректной работы приложения предусмотрены следующие VARSы:   
| Название                    | Значение по умолчанию | Описание                                    |
|-----------------------------|-----------------------|---------------------------------------------|
| `php_version`               | ` php8.1`              | версия php                                  |
| `user_name`             | `ubuntu`         | имя пользователя линукс |
| `user_group`            | `ubuntu`       | имя группы пользователя линукс             |
| `backup_database_folder`       | `~/backup` | директория для хранения бэкапов базы данных                 |  
| `phpmyadmin_directory`               | `/usr/share/`        | Путь до приложения phpmyadmin                  |
| `phpmyadmin_config_file`              | `/etc/phpmyadmin/config.inc.php`                | путь до конфигфайла phpmyadmin                                |
| `phpmyadmin_mysql_host`             | `{{ address_db01 }}`                 | адрес сервера базы данных (получаемый при создании инфрастуктуры)                            |
| `phpmyadmin_mysql_port`           | `3306`         | порт на котором расположена база данных                                |
| `phpmyadmin_mysql_socket`           | `""`       | соккет                          |
| `phpmyadmin_mysql_connect_type`           | `tcp`| протокол      | 
| `phpmyadmin_mysql_user`           | `{{ b_db_username }}`     | имя пользователя базы данных                     |
| `phpmyadmin_mysql_password`           | `{{ b_db_password }}`     | пароль к базе данных openvpn                     |
| `b_db_host`                   | `{{ ansible_host }}`         | Имя пользователя базы данных                |
| `b_db_u_host`               | `%`           | Доступ к базе данных вне сети             |		
| `b_db_username`             | `myproject`               | Имя пользователя базы данных                             |
| `user_name`                   | `ubuntu`       | Имя пользователя линукс              |		
| `b_db_database`                   | `myproject`                | имя базы данных данных                            |		
| `b_db_password`        | `1y99u4ff2HBssL3gAdK`                  | Пароль входа в базу данных                          |		
| `mysql_root_new_password`        | `Dz9V5hBkGhA7YkhTK2` | Root пароль к базе данных            |		
| `backup_database_host`       | `{{ address_backup01 }}`| Сервер бэкапов базы данных            |
| `mysql_packages`       | `mysql-server,  mysql-client,  python3-pymysql`| устанавливаемые приложения базы данных                             |
| `mysql_config_fie`       | `/etc/mysql/mysql.conf.d/mysqld.cnf`| адрес конфигфайла базы данных                             |		
| `git_server_address`       | `http://gitlab.com/Ramil1986/piwigo_server.git`| адрес репозитория с приложением Piwigo                             |
| `address_web01`       | `192.168.0.151`| адреc сервера с приложением  (получаемый при создании инфрастуктуры)                            |
| `address_db01`       | `192.168.0.152`| адреc сервера с базой данных  (получаемый при создании инфрастуктуры)                            |
| `address_db02`       | `192.168.0.154`| адреc сервера с базой данных  (получаемый при создании инфрастуктуры)                            |
| `address_backup01`       | `192.168.0.155`| адреc сервера бэкапов базы данных  (получаемый при создании инфрастуктуры)                            |

## Roles
backup - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/backup

grafana - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/grafana

my_sql - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/my_sql

mysql_exporter - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/mysql_exporter

Bnginx - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/nginx

nginx_exporter - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/nginx_exporter

phpmyadmin - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/phpmyadmin

piwigo - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/piwigo

prometheus - https://gitlab.com/ramil_nizamov1986/ansible/my-roles/prometheus

node_exporter -https://gitlab.com/ramil_nizamov1986/ansible/my-roles/node_exporter
