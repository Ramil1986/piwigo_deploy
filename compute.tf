

resource "yandex_compute_image" "ubuntu" {
  source_family = var.default_image
}

resource "yandex_compute_instance" "db" {
  count       = var.db_count
  hostname    = "${var.db_hostname}${format("%02d", count.index + 1)}"
  name        = "${var.db_hostname}${format("%02d", count.index + 1)}"
  platform_id = var.db_platform_id

  resources {
    cores         = var.db_cores
    memory        = var.db_memory
    core_fraction = var.db_core_fraction

  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = var.default_hdd_type
      size     = var.db_disk_size
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    "ssh-keys" = "${var.default_ssh_key_user}:${file("${var.default_ssh_key_path}")}"
  }
  
}

resource "yandex_compute_instance" "web" {
  count       = var.web_count
  hostname    = "${var.web_hostname}${format("%02d", count.index + 1)}"
  name        = "${var.web_hostname}${format("%02d", count.index + 1)}"
  platform_id = var.web_platform_id

  resources {
    cores         = var.web_cores
    memory        = var.web_memory
    core_fraction = var.web_core_fraction

  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = var.default_hdd_type
      size     = var.web_disk_size
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    "ssh-keys" = "${var.default_ssh_key_user}:${file("${var.default_ssh_key_path}")}"
  }
 
}

resource "yandex_compute_instance" "monitor" {
  count       = var.monitor_count
  hostname    = "${var.monitor_hostname}${format("%02d", count.index + 1)}"
  name        = "${var.monitor_hostname}${format("%02d", count.index + 1)}"
  platform_id = var.monitor_platform_id

  resources {
    cores         = var.monitor_cores
    memory        = var.monitor_memory
    core_fraction = var.monitor_core_fraction

  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = var.default_hdd_type
      size     = var.monitor_disk_size
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    "ssh-keys" = "${var.default_ssh_key_user}:${file("${var.default_ssh_key_path}")}"
  }
  
}

resource "yandex_compute_instance" "backup" {
  count       = var.backup_count
  hostname    = "${var.backup_hostname}${format("%02d", count.index + 1)}"
  name        = "${var.backup_hostname}${format("%02d", count.index + 1)}"
  platform_id = var.backup_platform_id

  resources {
    cores         = var.backup_cores
    memory        = var.backup_memory
    core_fraction = var.backup_core_fraction

  }

  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = yandex_compute_image.ubuntu.id
      type     = var.default_hdd_type
      size     = var.backup_disk_size
    }
  }

  network_interface {
    subnet_id          = yandex_vpc_subnet.subnet-1.id
    nat                = true
    security_group_ids = [yandex_vpc_security_group.alb-vm-sg.id]
  }

  metadata = {
    "ssh-keys" = "${var.default_ssh_key_user}:${file("${var.default_ssh_key_path}")}"
  }
 
}
