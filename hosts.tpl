[db]
%{ for addr in ip_addrsdb ~}
${addr}
%{ endfor ~}
[web]
%{ for addr in ip_addrsweb ~}
${addr}
%{ endfor ~}

[backup]
%{ for addr in ip_addrbackup ~}
${addr}
%{ endfor ~}

[mon]
%{ for addr in ip_addrmon ~}
${addr}
%{ endfor ~}



