resource "yandex_vpc_network" "piwigo_network" {
  name = var.piwigo_network
}

resource "yandex_vpc_subnet" "subnet-1" {
  folder_id      = var.folder_id
  name           = var.subnet_name
  zone           = var.yc_zone
  network_id     = yandex_vpc_network.piwigo_network.id
  v4_cidr_blocks = var.subnet_v4_cidr_bloks
}

resource "yandex_vpc_security_group" "alb-sg" {
  name        = "alb-sg"
  network_id  = yandex_vpc_network.piwigo_network.id

  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol       = "TCP"
    description    = "ext-http"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 80
  }

  ingress {
    protocol       = "TCP"
    description    = "ext-https"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 443
  }

}

resource "yandex_vpc_security_group" "alb-vm-sg" {
  name        = "alb-vm-sg"
  network_id  = yandex_vpc_network.piwigo_network.id
  egress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol       = "ANY"
    description    = "any"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 1
    to_port        = 65535
  }
  ingress {
    protocol          = "TCP"
    description       = "TCP"
    security_group_id = yandex_vpc_security_group.alb-sg.id
    port              = 80
  }
  ingress {
    protocol          = "TCP"
    description       = "TCP"
    security_group_id = yandex_vpc_security_group.alb-sg.id
    port              = 443
  }

  ingress {
    protocol       = "TCP"
    description    = "ssh"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 22
  }

}
