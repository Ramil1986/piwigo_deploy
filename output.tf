resource "local_file" "inventory" {
  filename = "hosts"
  file_permission = "0644"
  
  content = templatefile("hosts.tpl", {
    ip_addrsdb = yandex_compute_instance.db[*].network_interface.0.nat_ip_address
    ip_addrsweb = yandex_compute_instance.web[*].network_interface.0.nat_ip_address
    ip_addrbackup = yandex_compute_instance.backup[*].network_interface.0.nat_ip_address
    ip_addrmon = yandex_compute_instance.monitor[*].network_interface.0.nat_ip_address
  })
}
resource "local_file" "vars" {
  filename = "vars.yml"
  file_permission = "0644"
  
  content = templatefile("vars.tpl", {
    ip_addrsdb = [ 
      for item in yandex_compute_instance.db[*]:
      "${item.name}: ${item.network_interface.0.ip_address}"
    ]
    ip_addrsbackup = [ 
      for item in yandex_compute_instance.backup[*]:
      "${item.name}: ${item.network_interface.0.ip_address}"
    ]
    ip_addrsweb = [ 
      for item in yandex_compute_instance.web[*]:
      "${item.name}: ${item.network_interface.0.ip_address}"
    ]    
  })
}

output "internal_ip_address_monitor" {
  value = yandex_compute_instance.monitor[*].network_interface.0.nat_ip_address
}

output "internal_ip_address_web" {
  value = yandex_compute_instance.web[*].network_interface.0.nat_ip_address
}