# Настройка провайдера

terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.47.0"
    }
  } 
  backend "s3" {
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
      dynamodb = "https://docapi.serverless.yandexcloud.net/ru-central1/b1glra0lkuq1ul8cjp3h/etnocbauin896g9ghbqs" 
    }
    access_key = ""
    secret_key = ""

    region = "ru-central1"
    bucket = "piwigo-tf-state"
    key = "tf.state"
    skip_region_validation = true
    skip_metadata_api_check = true
    skip_credentials_validation = true
    skip_requesting_account_id = true
    skip_s3_checksum = true
    use_path_style = true
    dynamodb_table = "piwigo-tf-state"
  } 
}

provider "yandex" {
  zone = "ru-central1-a"
  cloud_id = var.cloud_id
  folder_id = var.folder_id
  token = var.token
}




